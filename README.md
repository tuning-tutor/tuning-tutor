# Tuning Tutor

Nicholas Coltharp
Friday, June 14, 2023

## Project

Theoretically, this is meant to be program that listens to a (simple) piece of music, matches it against a (simple) description of that music (in the form of an ABC file), and determines how accurately the piece was played.

In practice, I was not able to find a way to make this work.  I do have a little interactive tuner, though.

## Dependencies

- Python 3.10 or later.
- These Python packages:
  + NumPy
  + SciPy
  + `sounddevice`
  + `tqdm`
  + Parsita

If you have Nix, you can run `nix develop` to get a shell with all dependencies available.

## Programs

- `tuning_tutor.parrot` reads in a WAV file, decomposes it using the FFT, and then recombines it and plays it back to specified precision.  This is meant to test the pitch analysis algorithm.  Run it like `python -m tuning_tutor.parrot WAV`.
- `tuning_tutor.abcfile`, when run as a program, reads an ABC file and dumps the parsed result to output.  Run it like `python -m tuning_tutor.abcfile ABC`.
- `tuning_tutor.pitch`, when run as a program, reads an ABC file and plays it back.  Run it like `python pitch.py ABC`.
- `tuning_tutor.tune_interactive` is an interactive curses-based tuner.  Late in the development , when it became clear that the original idea wasn't working, I started developing this to get a better idea of what was going wrong.  Run it like `python -m tuning_tutor.tune_interactive`.
- `tuning_tutor.tune` is my best-effort attempt at the original tuning idea.  Run it like `python -m tuning_tuner.tune ABC WAV`.

There is an example ABC file `examples/sarabande.abc`.  This is the sarabande from Bach's Cello Suite No. 5, chosen because it is generally played at a slow-ish tempo, and because it is probably the most famous monophonic piece in the standard cello repertoire.  There is also an example WAV recording of the same piece, `examples/sarabande.wav`.  Any poor tuning and stylistic infidelities are, of course, purely for the sake of testing the program; nonetheless, if you want a better recording, I recommend Ophélie Gaillard.

Several programs take a temperament as an optional argument.  I don't yet have a principled way to do this, so I'm just using `eval` for now.  Standard concert tuning is given by `"EqualTemperament(69, 440)"`.

## Future Work

- Give a more principled treatment of ABC files, general music concepts, and a layer to translate between them.
- The iterative sample-by-sample note-matching algorithm is probably not going to work, because it is globally sensitive to local errors.  This is why I originally wanted to use a string-distance metric: the whole purpose of a string-distance metric is essentially to deal with local differences in a way that doesn't cause problems globally.  However, I wasn't able to find an existing string-distance metric that would work for my purposes.  (Also, most Python string-distance libraries seem to require the use of actual `str` objects rather than accepting any type of sequence, which could be problematic.)  Any candidate metric needs to be able to tolerate a single note being "interrupted" due to noise; as far as I can tell, no such metric has been developed.  I think such it must be possible, but it will have to be designed specifically for this purpose.  It might also need some domain-specific information; e.g., it might want to know that octaves are "close" to each other.
- We could simply construct a filter bank rather than brute-forcing with the FFT.  Actually, having an ABC file available to us means that we can tailor our filter bank to the song at hand---just use one bandpass filter for each note we expect to see.
- But really, the next thing I want to try is some sort of autocorrelation method.

## Miscellaneous Thoughts

- Naming is a **huge** problem.  The English words “note”, “tone”, and “pitch” all have different meanings in different contexts; at the same time, they are often used interchangeably.  In particular, “note” has multiple meanings within essentially *the same* context; its precise meaning varies micro-contextually.  I’m not satisfied my names, but I haven’t had time to think hard about them.
- On a similar note, human music concepts and notations are super messy.  I have a lot of dumb ad-hoc code dealing with note names and accidentals.
- I haven't had to write anything in Python other than one-off scripts for a while.  Coming back to it, I'm finding that even medium-scale program organization is a hassle.  I don't miss it.
- On a similar note, it's really annoying to have several programs that take partially-but-not-entirely overlapping command-line arguments.  I really hate this kind of code duplication, but any attempt I made to unify these ended up seeming brittle and over-engineered.
