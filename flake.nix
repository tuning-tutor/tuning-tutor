{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem (system: let
    pkgs = nixpkgs.legacyPackages.${system};
    parsita = pyPkgs: pyPkgs.buildPythonPackage rec {
      pname = "parsita";
      version = "1.7.2";
      format = "pyproject";
      src = pyPkgs.fetchPypi {
        inherit pname version;
        hash = "sha256-g5Z+zbKQ+bf/ZdyUbm6umkJsnS3JD2iSVutIvIEiCWU=";
      };
      nativeBuildInputs = [
        pyPkgs.poetry-core
      ];
    };
    devPackages = [
        pkgs.nil
        pkgs.python3Packages.python-lsp-server
        pkgs.python3Packages.jedi
        pkgs.python3Packages.rope
        pkgs.python3Packages.pyflakes
        pkgs.python3Packages.pycodestyle
        pkgs.python3Packages.pydocstyle
        pkgs.python3Packages.pylint
        pkgs.python3Packages.pylsp-mypy
        pkgs.python3Packages.python-lsp-black
        pkgs.python3Packages.pyls-isort
        pkgs.python3Packages.python-lsp-ruff
        pkgs.python3Packages.ipython
        (pkgs.python3.withPackages (pkgs: [
          pkgs.numpy
          pkgs.scipy
          pkgs.sounddevice
          pkgs.tqdm
          (parsita pkgs)
        ]))
      ];
  in {
    devShells.default = pkgs.mkShell {
      packages = devPackages;
    };

    devShells.ipython = pkgs.mkShell {
      packages = devPackages;
      shellHook = ''
        exec ipython -i -c '
          import numpy as np
          from numpy import *
        '
      '';
    };
  });
}
