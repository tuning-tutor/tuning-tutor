from typing import Any, Tuple

import numpy as np
import sounddevice as sd
from numpy.typing import DTypeLike, NDArray
from scipy.io import wavfile


def _sample_limits(dtype: DTypeLike) -> Tuple[Any, Any]:
    match dtype:
        case np.uint8 | np.int16:
            iinfo = np.iinfo(dtype)
            return iinfo.min, iinfo.max
        case np.float32 | np.float64:
            return (-1, 1)
        case _:
            raise ValueError


def read_wav(filename: str) -> Tuple[NDArray, int]:
    """Read a WAV file into a NumPy array.

    Returns an array of dtype `float64`.

    The returned array will always have shape `(samples, channels)`, even if the
    input file has only one channel.

    """
    rate, data = wavfile.read(filename)
    (minimum, maximum) = _sample_limits(data.dtype)
    data = data.astype(np.float64)
    data = (data - minimum) * 2 / (maximum - minimum) - 1
    # Ensure the data is two-dimensional.  This helps simplify things elsewhere.
    data = data.reshape((len(data), -1))
    return data, rate


def write_wav(filename: str, rate: int, data: NDArray) -> None:
    """Write a NumPy array to a WAV file."""
    (minimum, maximum) = _sample_limits(data.dtype)
    # If the data is out-of-bounds, a mistake has almost certainly been made
    # somewhere.
    assert minimum <= data.min() <= maximum
    wavfile.write(filename, rate, data)


def play_sound(data: NDArray, rate: int) -> None:
    """Play a NumPy array as sound."""
    (minimum, maximum) = _sample_limits(data.dtype)
    assert minimum <= data.min() <= maximum
    sd.play(data, rate, blocking=True)
