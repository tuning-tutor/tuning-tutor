"""Rudimentary ABC file parsing and manipulation."""

# NOTE Abstract base classes, NOT the ABC file format!
from collections.abc import Iterable
from dataclasses import dataclass
from fractions import Fraction
from typing import Literal, Optional, Tuple

from parsita import TextParsers, eof, lit, opt, reg, rep, repsep
from parsita.util import splat

import tuning_tutor.pitch as pitch
import tuning_tutor.scale as scale
from tuning_tutor.scale import NoteName

KeyAccidental = Optional[Literal["b", "#"]]


def _key_accidental_to_scale(accidental: KeyAccidental) -> scale.KeyAccidental:
    match accidental:
        case "b":
            return scale.KeyAccidental("♭")
        case None:
            return scale.KeyAccidental(None)
        case "#":
            return scale.KeyAccidental("♯")


# Accidentals for notes. "_" is flat, "=" is natural, "^" is sharp.  "__" and
# "^^" are double flat and double sharp respectively.
NoteAccidental = Literal["__", "_", "=", "^", "^^"]

Mode = Literal["major", "minor"]


@dataclass
class Key:
    """A musical key."""

    name: NoteName
    accidental: Optional[KeyAccidental]
    mode: Mode

    @staticmethod
    def _from_parts(
        name: NoteName, accidental: list[KeyAccidental], mode: list[Mode]
    ) -> "Key":
        """Turn the parsed components of an ABC key into a Key object."""
        match accidental:
            case []:
                ret_accidental = None
            case [a]:
                ret_accidental = a
            case _:
                raise ValueError(f"invalid accidental part: {accidental}")

        ret_mode: Mode
        match mode:
            case []:
                ret_mode = "major"
            case [m]:
                ret_mode = m
            case _:
                raise ValueError(f"invalid mode part: {mode}")

        return Key(name, ret_accidental, ret_mode)

    # Compute accidentals.  This approach would need to be modified somewhat if
    # we were to add support for other modes.

    @property
    def accidentals(self) -> dict[NoteName, NoteAccidental]:
        """The accidentals in this key."""
        base = (
            scale.Fifth(scale.NoteAccidental("♮"), scale.Note("C"))
            if self.mode == "major"
            else scale.Fifth(scale.NoteAccidental("♮"), scale.Note("A"))
        )
        # Our location on the circle of scale.
        fifth = scale.Fifth(
            _key_accidental_to_scale(self.accidental).to_note_accidental(),
            scale.Note(self.name),
        )
        diff = fifth - base
        if diff < 0:
            # Flat key
            diff = abs(diff)
            return dict((note, "_") for note in scale.FLATS[:diff])
        elif diff > 0:
            # Sharp key
            return dict((note, "^") for note in scale.SHARPS[:diff])
        else:
            return {}


# Note names in ABC (Helmholtz) notation.  Uppercase letters signify "low"
# notes; lowercase letters signify "high" notes.
ABCNoteName = Literal[
    "A", "B", "C", "D", "E", "F", "G", "a", "b", "c", "d", "e", "f", "g"
]


def _abc_note_name_to_note_name(name: ABCNoteName) -> NoteName:
    return name.upper()  # type: ignore[return-value]


@dataclass
class Note:
    """A note in ABC notation, consisting of an optional accidental, a note
    name, an octave, and a duration.

    """

    accidental: Optional[NoteAccidental]
    name: ABCNoteName
    octave: int
    duration: Fraction

    @staticmethod
    def _from_parts(
        accidental: list[NoteAccidental],
        name: ABCNoteName,
        octaves: str,
        duration: list[Tuple[str, list[str]]],
    ) -> "Note":
        """Turn the parsed components of an ABC note into a Note object."""
        # Extract the accidental, if any.
        match accidental:
            case []:
                ret_accidental = None
            case [a]:
                ret_accidental = a
            case _:
                raise ValueError(f"invalid accidental part: {accidental}")

        # Compute the octave.
        if not (
            name.isupper()
            and all(c == "," for c in octaves)
            or name.islower()
            and all(c == "'" for c in octaves)
        ):
            raise ValueError(f"invalid name/octaves part: {name}, {octaves}")
        octave = len(octaves)

        # Compute the duration.
        match duration:
            case []:
                ret_duration = Fraction(1)
            case [(n, [])]:
                ret_duration = Fraction(int(n))
            case [(n, [d])]:
                ret_duration = Fraction(int(n), int(d))
            case _:
                raise ValueError(f"invalid duration part: {duration}")

        return Note(ret_accidental, name, octave, ret_duration)

    def to_offset(self, accidentals: dict[NoteName, NoteAccidental]) -> pitch.Note:
        if self.name.isupper():
            p = 60 - 12 * self.octave
        else:
            p = 72 + 12 * self.octave
        p += scale.DEGREES[_abc_note_name_to_note_name(self.name)]
        accidental = self.accidental or accidentals.get(
            _abc_note_name_to_note_name(self.name)
        )
        match accidental:
            case "__":
                p -= 2
            case "_":
                p -= 1
            case "^":
                p += 1
            case "^^":
                p += 2
        return pitch.Note(p, self.duration)


# An element that may appear in a tune.
#
# - "|" is a measure bar.
#
# - "|:" is the start of a repeated section.
#
# - ":|" is a repeat.
#
# - "::" is both a repeat and the start of a repeat.
TuneElement = Literal["|", "|:", ":|", "::"] | list[Note]

Element = Key | TuneElement


def is_bar(elem: Element) -> bool:
    """Indicates whether an ABC element is a measure bar."""
    return elem in ["|", ":|", "|:", "::"]


def is_repeat_begin(elem: Element) -> bool:
    """Indicates whether an ABC element is the beginning of a repeat."""
    return elem in ["|:", "::"]


def is_repeat_end(elem: Element) -> bool:
    """Indicates whether an ABC element is the end of a repeat."""
    return elem in [":|", "::"]


@dataclass
class ABCFile:
    """An ABC music file."""

    elements: list[Element]

    @staticmethod
    def _from_parts(parts: list[Key | list[TuneElement]]) -> "ABCFile":
        """Construct an ABCFile object from parsed components."""
        elements: list[Element] = []
        for part in parts:
            match part:
                case Key(_):
                    elements.append(part)
                case _:
                    elements.extend(part)
        return ABCFile(elements)

    @property
    def offsets(self) -> Iterable[pitch.Note]:
        """Converts an ABC file to a sequence of offset-based notes."""
        base_accidentals = {}
        accidentals = {}
        repeat = []
        for elem in self.elements:
            if isinstance(elem, Key):
                base_accidentals = elem.accidentals
                accidentals = base_accidentals.copy()
                continue
            if isinstance(elem, list):  # Must be a list of notes.
                for note in elem:
                    p = note.to_offset(accidentals)
                    yield p
                    repeat.append(p)
                    if note.accidental:
                        accidentals[
                            _abc_note_name_to_note_name(note.name)
                        ] = note.accidental
                continue
            # Reset accidentals at measure boundaries.
            if is_bar(elem):
                accidentals = base_accidentals.copy()
            if is_repeat_end(elem):
                yield from repeat
            if is_repeat_begin(elem):
                repeat = []


class ABCParsers(TextParsers, whitespace=None):  # type: ignore[call-arg]
    """Holding area for ABC file parser combinators."""

    # Whitespace that has no newlines.
    ws = reg("[\s&&^\n]*")
    ws1 = reg("[\s&&^\n]+")

    @staticmethod
    def field(name: str, parser):
        return lit(name) >> lit(":") >> parser

    key_name = reg("[A-G]")
    key_accidental = lit("b", "#")
    mode = lit("major", "minor")
    key = field("K", key_name & opt(key_accidental) & opt(ws1 >> mode)) > splat(
        Key._from_parts
    )

    note_accidental = opt(lit("__", "_", "^^", "^", "="))
    note_name = reg("[A-Ga-g]")
    # Octaves: empty string is allowed, but only after the first alternative has
    # been checked.
    octaves = reg(",+|'*")
    duration = opt(reg("[0-9]+") & opt(lit("/") >> reg("[0-9]")))
    note = note_accidental & note_name & octaves & duration > splat(Note._from_parts)

    tune_element = lit("|", "|:", ":|", "::") | rep(note, min=1)

    field_line = key
    tune_line = ws >> repsep(tune_element, ws1) << ws
    line = field_line | tune_line

    abc = (repsep(line, lit("\n")) << eof) > ABCFile._from_parts


def parse(s: str):
    """Parse a string to an ABC file."""
    return ABCParsers.abc.parse(s)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Parse an ABC music file.")
    parser.add_argument("filename", metavar="ABC", help="ABC file to parse", type=str)
    args = parser.parse_args()
    print(parse(open(args.filename).read()).or_die())
