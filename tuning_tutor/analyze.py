"""Core frequency-analysis routines."""

from argparse import Namespace
from collections.abc import Iterable
from typing import Tuple

import numpy as np
import scipy
from numpy.typing import NDArray
from tqdm import tqdm


def guess_frequencies_block(
    data: NDArray,
    rate: int | NDArray,
    num_freqs: int = 1,
    window: str | NDArray = "hamming",
) -> Tuple[NDArray, NDArray]:
    """Find the N frequencies in input data with the highest amplitude.

    This is like `guess_frequencies`, except it operates over the entire input
    array all at once.

    `rate` may be an array, in which case it is interpreted as an array of
    frequency bins.

    """
    win = (
        scipy.signal.windows.get_window(window, len(data))
        if isinstance(window, str)
        else window
    )
    freqs = scipy.fft.fftfreq(len(data), 1 / rate) if isinstance(rate, int) else rate
    transformed = scipy.fft.fft(win * data, axis=0).real
    ixs = np.argpartition(transformed, -num_freqs, axis=0)
    ixs = ixs[-num_freqs:]
    return np.abs(freqs.take(ixs)), transformed.take(ixs)


def get_window_size(rate: int, resolution: float) -> int:
    """Get the window size for a given sample rate and frequency resolution."""
    return 2 ** int(np.ceil(np.log2(rate / resolution)))

def guess_frequencies(
    data: NDArray,
    rate: int,
    resolution: float,
    stride: int = 1,
    num_freqs: int = 1,
    window: str | NDArray = "hamming",
    display_progress: bool = False,
) -> Iterable[Tuple[NDArray, NDArray]]:
    """Find the N frequencies in input data with the highest amplitude.

    The input array is expected to have shape `(samples, *rest)`.

    Other arguments:

    - `rate` is the sample rate.
    - `resolution` is the minimum desired frequency resolution.
    - `stride` is the interval at which windows will be sampled.
    - `num_freqs` is the number of frequencies to find.
    - `window` is the windowing function to use.  If it is an array, then it
      will be used directly; if it is a string, it is interpreted as a window
      name.
    - If `display_progress` is true, this function will automatically display a
      nice progress bar.

    Returns a generator that yields pairs `(freqs, amps)`, where `freqs` is an
    array of the N frequencies with the highest amplitudes in a given window,
    and `amps` is their corresponding amplitudes.  `freqs` and `amps` both have
    shape `(num_frequencies, skip, *rest)`.

    Note that the frequencies in `freqs` and `amps` are not necessarily stored
    in any particular order; i.e., they are not necessarily sorted in order of
    amplitude.

    """
    window_size = get_window_size(rate, resolution)
    win = (
        scipy.signal.windows.get_window(window, window_size)
        if isinstance(window, str)
        else window
    )
    # Make the window have the same number of dimensions as the input.
    win = win.reshape((-1, *((data.ndim - 1) * (1,))))
    freqs = scipy.fft.fftfreq(window_size, 1 / rate)
    n = len(data) // stride
    it = range(n)
    if display_progress:
        it = tqdm(it, desc="Guessing frequencies")
    for i in it:
        # TODO Can we do better than uniform sampling?
        ix = i * stride
        w = data[ix : ix + window_size]
        if len(w) < window_size:
            # TODO This just appends zeros at the end.  Other options?
            w = np.append(w, np.zeros((window_size - len(w), *w.shape[1:])), axis=0)
        yield guess_frequencies_block(w, freqs, num_freqs, win)


def guess_frequencies_from_args(
    data: NDArray,
    rate: int,
    args: Namespace,
    display_progress: bool = False,
) -> Tuple[int, Iterable[Tuple[NDArray, NDArray]]]:
    """Find the highest frequencies in data, given common program args.

    Returns a pair `(stride, results)`, where `stride` is the stride at which
    windows were sampled, and `results` is an iterable containing the output of
    `guess_frequencies`.

    """
    stride = int(np.floor(rate * args.time_resolution))
    return stride, guess_frequencies(
        data,
        rate,
        args.frequency_resolution,
        stride,
        args.num_frequencies,
        args.window,
        display_progress,
    )
