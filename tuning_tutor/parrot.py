"""Play back a WAV file's loudest frequencies."""

from argparse import ArgumentParser

import numpy as np
from tqdm import tqdm

import tuning_tutor
from tuning_tutor import play_sound, read_wav
from tuning_tutor.analyze import guess_frequencies_from_args
from tuning_tutor.pitch.temperament import Temperament

parser = ArgumentParser(
    description="Play back a WAV file's loudest frequencies.",
)
parser.add_argument("infile", metavar="WAV", help="WAV file to parrot", type=read_wav)
parser.add_argument(
    "--frequency-resolution",
    "-f",
    metavar="HZ",
    help="Minimum frequency resolution in Hz",
    type=float,
    default=1.0,
)
parser.add_argument(
    "--window",
    "-w",
    metavar="NAME",
    help="Windowing function to use",
    type=str,
    default="hamming",
)
parser.add_argument(
    "--temperament",
    "-T",
    metavar="TEMPERAMENT",
    help="Temperament to use",
    type=str,
    default=None,
)
parser.add_argument(
    "--attack",
    "-a",
    metavar="PROPORTION",
    help="Proportion of window time to spend interpolating amplitudes",
    type=float,
    default=0.25,
)
parser.add_argument(
    "--num-frequencies",
    "-n",
    metavar="NUM",
    help="Number of frequencies",
    type=int,
    default=1,
)
parser.add_argument(
    "--time-resolution",
    "-t",
    metavar="SECS",
    help="Minimum time resolution in seconds",
    type=float,
    default=0.1,
)
parser.add_argument(
    "--sort-frequencies",
    "-s",
    help="Sort frequencies before interpolating amplitudes",
    action="store_true",
)
parser.add_argument(
    "--mono",
    "-m",
    help="Output in mono",
    action="store_true",
)
args = parser.parse_args()

data, rate = args.infile

if args.mono:
    data = data.mean(axis=1).reshape(-1, 1)
nchannels = data.shape[1]

# TODO Do processing on-the-fly rather than all up front.
stride, results_iter = guess_frequencies_from_args(
    data, rate, args, display_progress=True
)
results = list(results_iter)

# TODO Better ways to do this than linear interpolation?
attack_end = int(np.round(args.attack * stride))
temperament: Temperament = args.temperament and eval(
    args.temperament, vars(tuning_tutor.pitch.temperament)
)

old_amps = np.zeros((args.num_frequencies, nchannels))
phase = np.zeros((args.num_frequencies, nchannels))
out_data = np.zeros_like(data)
for i, (freqs, amps) in tqdm(
    enumerate(results), desc="Crunching frequencies", total=len(results)
):
    ix = i * stride
    if args.sort_frequencies:
        ixs = np.argsort(freqs, axis=0)
        freqs = freqs.take(ixs)
        amps = amps.take(ixs)
    if temperament:
        freqs = temperament.snap(freqs)
    sample_space = np.linspace(0, args.time_resolution * 2 * np.pi, num=stride)
    sample_space = (
        phase[:, np.newaxis, :]
        + freqs[:, np.newaxis, :] * sample_space[np.newaxis, :, np.newaxis]
    )
    out_amps = amps[:, np.newaxis, :].repeat(stride, axis=1)
    out_amps[:, :attack_end] = np.linspace(old_amps, amps, num=attack_end).transpose(
        (1, 0, 2)
    )
    out_data[ix : ix + stride] = (out_amps * np.sin(sample_space)).mean(axis=0)
    old_amps = amps
    phase = sample_space[:, -1, :]
out_data /= np.abs(out_data).max()
play_sound(out_data, rate)
