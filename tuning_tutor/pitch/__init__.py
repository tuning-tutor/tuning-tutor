"""Basic framework for manipulating music as a sequence of pitches.

"""
from dataclasses import dataclass
from fractions import Fraction


@dataclass
class Note:
    """A note in scientific notation.  The offset indicates how many semitones
    the note is from C(-1).

    """

    offset: int
    duration: Fraction
