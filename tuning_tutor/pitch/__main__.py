import argparse
from argparse import ArgumentParser
from sys import stdin

from tuning_tutor.pitch import temperament as temp
import tuning_tutor.abcfile as abcfile
from tuning_tutor import play_sound, write_wav

parser = ArgumentParser(description="Synthesize an ABC music file.")
parser.add_argument(
    "infile",
    metavar="INFILE",
    nargs="?",
    help="ABC file to synthesize",
    type=argparse.FileType("r"),
    default=stdin,
)
parser.add_argument(
    "--out",
    "-o",
    metavar="OUTFILE",
    help="WAV file to write to (if missing, will play sound)",
    type=argparse.FileType("wb"),
    nargs="?",
    default=None,
)
parser.add_argument(
    "--tempo",
    "-t",
    metavar="BPM",
    help="Tempo to play at",
    type=float,
    default=100.0,
)
parser.add_argument(
    "--rate", "-r", metavar="RATE", help="Sample rate", type=int, default=48000
)
parser.add_argument(
    "--temperament",
    metavar="EXPR",
    help="Temperament to play at",
    type=str,
    default="EqualTemperament(69, 440)",
)
args = parser.parse_args()
music = abcfile.parse(args.infile.read()).or_die()
temperament = eval(args.temperament, vars(temp))
sound = temperament.to_sound(args.rate, args.tempo, list(music.offsets))
if args.out:
    write_wav(args.out, args.rate, sound)
else:
    play_sound(sound, args.rate)
