from abc import ABC, abstractmethod
from dataclasses import dataclass
from itertools import accumulate
from typing import overload

import numpy as np
from numpy.typing import NDArray
from tuning_tutor.pitch import Note


class Temperament(ABC):
    """A temperament, which maps note offsets to frequencies."""

    @overload
    def to_pitch(self, offset: int) -> float:
        pass

    @overload
    def to_pitch(self, offset: NDArray) -> NDArray:
        pass

    @abstractmethod
    def to_pitch(self, offset: int | NDArray) -> float | NDArray:
        """Convert a note offset to a pitch."""
        pass

    @overload
    def guess_offset(self, pitch: float) -> int:
        pass

    @overload
    def guess_offset(self, pitch: NDArray) -> NDArray:
        pass

    @abstractmethod
    def guess_offset(self, pitch: float | NDArray) -> int | NDArray:
        """Given a pitch, guess the note offset that it corresponds to.  This
        should be a left inverse of to_pitch.

        """
        pass

    @overload
    def snap(self, pitch: int) -> int:
        pass

    @overload
    def snap(self, pitch: NDArray) -> NDArray:
        pass

    def snap(self, pitch: float | NDArray) -> float | NDArray:
        """Snap a pitch to this temperament."""
        return self.to_pitch(self.guess_offset(pitch))

    def to_sound(self, rate: int, bpm: float, notes: list[Note]) -> NDArray:
        """Convert a list of ABC notes to a list of pitches in this temperament."""
        bps = 60 / bpm
        tempo = 60 * rate / bpm
        durs = [int(round(tempo * note.duration)) for note in notes]
        sums = list(accumulate(durs))
        ret = np.empty(sums[-1])
        starts = [0] + sums[:-1]
        phase = 0
        for note, start, dur in zip(notes, starts, durs):
            dur = int(round(tempo * note.duration))
            sample_space = np.linspace(
                phase,
                phase + note.duration * bps * self.to_pitch(note.offset) * 2 * np.pi,
                num=dur,
            )
            ret[start : start + dur] = np.sin(sample_space)
            phase = sample_space[-1]
        return ret


@dataclass
class EqualTemperament(Temperament):
    """Equal temperament, specified by a reference note and a corresponding
    reference pitch.

    """

    reference_note: int
    reference_pitch: float

    @overload
    def to_pitch(self, offset: int) -> float:
        pass

    @overload
    def to_pitch(self, offset: NDArray) -> NDArray:
        pass

    def to_pitch(self, offset: int | NDArray) -> float | NDArray:
        return self.reference_pitch * 2 ** ((offset - self.reference_note) / 12)

    @overload
    def guess_offset(self, pitch: float) -> int:
        pass

    @overload
    def guess_offset(self, pitch: NDArray) -> NDArray:
        pass

    def guess_offset(self, pitch: float | NDArray) -> int | NDArray:
        return np.round(
            self.reference_note - 12 * np.log2(self.reference_pitch / pitch)
        ).astype(np.int64)
