"""Rudimentary support for queries about musical scales."""

from dataclasses import dataclass
from typing import Literal, Optional

AccidentalName = Literal["𝄫", "♭", "♮", "♯", "𝄪"]

ACCIDENTAL_NAMES = ["𝄫", "♭", "♮", "♯", "𝄪"]

KeyAccidentalName = Optional[Literal["♭", "♯"]]

KEY_ACCIDENTAL_NAMES: list[KeyAccidentalName] = ["♭", None, "♯"]

NoteName = Literal[
    "C",
    "D",
    "E",
    "F",
    "G",
    "A",
    "B",
]


DEGREES: dict[NoteName, int] = {
    "C": 0,
    "D": 2,
    "E": 4,
    "F": 5,
    "G": 7,
    "A": 9,
    "B": 11,
}

REVERSE_DEGREES: dict[int, NoteName] = {
    0: "C",
    2: "D",
    4: "E",
    5: "F",
    7: "G",
    9: "A",
    11: "B",
}

NOTES: list[NoteName] = [
    "C",
    "G",
    "D",
    "A",
    "E",
    "B",
    "F",
]
FLATS = NOTES[-2::-1] + [NOTES[-1]]
SHARPS = list(reversed(FLATS))


@dataclass
class KeyAccidental:
    name: KeyAccidentalName

    def __sub__(self, other: "KeyAccidental") -> int:
        return KEY_ACCIDENTAL_NAMES.index(self.name) - KEY_ACCIDENTAL_NAMES.index(
            other.name
        )

    def to_note_accidental(self) -> "NoteAccidental":
        name = self.name or "♮"
        return NoteAccidental(name)


@dataclass
class NoteAccidental:
    name: AccidentalName

    def __sub__(self, other: "NoteAccidental") -> int:
        return ACCIDENTAL_NAMES.index(self.name) - ACCIDENTAL_NAMES.index(other.name)


@dataclass
class Note:
    name: NoteName

    def __sub__(self, other: "Note") -> int:
        return NOTES.index(self.name) - NOTES.index(other.name)


@dataclass
class Fifth:
    """A representation of a place on the circle of fifths as a pair
    (accidental, note).  Two places can be subtracted to get the distance
    between them as an integer.

    """

    accidental: NoteAccidental
    note: Note

    def __sub__(self, other: "Fifth") -> int:
        return (self.accidental - other.accidental) * len(NOTES) + (
            self.note - other.note
        )


@dataclass
class Pitch:
    note: Note
    accidental: NoteAccidental
    octave: int

    @staticmethod
    def from_offset(offset: int) -> list["Pitch"]:
        """Given an offset from C(-1), return a list of corresponding Pitch
        objects.  Double-flat and double-sharp accidentals are NOT included.
        """
        octave = offset // 12 - 1
        degree = offset % 12
        name = REVERSE_DEGREES.get(degree)
        if name:
            return [Pitch(Note(name), NoteAccidental("♮"), octave)]
        else:
            return [
                Pitch(Note(REVERSE_DEGREES[degree - 1]), NoteAccidental("♯"), octave),
                Pitch(Note(REVERSE_DEGREES[degree + 1]), NoteAccidental("♭"), octave),
            ]

    def display(self, include_natural=False):
        ret = self.note.name
        if self.accidental.name != "♮" or include_natural:
            ret += self.accidental.name
        ret += str(self.octave)
        return ret
