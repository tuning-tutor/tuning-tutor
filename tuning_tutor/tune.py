"""Judge the tuning in a WAV file according to an ABC file."""

from argparse import ArgumentParser

import numpy as np

import tuning_tutor
from tuning_tutor import abcfile, read_wav
from tuning_tutor.analyze import guess_frequencies_from_args
from tuning_tutor.pitch.temperament import Temperament

parser = ArgumentParser(
    description="Tune",
)
parser.add_argument("abcfile", metavar="ABC", help="ABC file", type=str)
parser.add_argument("wavfile", metavar="WAV", help="WAV file", type=str)
parser.add_argument(
    "--frequency-resolution",
    "-f",
    metavar="HZ",
    help="Minimum frequency resolution in Hz",
    type=float,
    default=1.0,
)
parser.add_argument(
    "--window",
    "-w",
    metavar="NAME",
    help="Windowing function to use",
    type=str,
    default="hamming",
)
parser.add_argument(
    "--temperament",
    "-T",
    metavar="TEMPERAMENT",
    help="Temperament to use",
    type=str,
    default="EqualTemperament(69, 440)",
)
parser.add_argument(
    "--num-frequencies",
    "-n",
    metavar="NUM",
    help="Number of frequencies",
    type=int,
    default=1,
)
parser.add_argument(
    "--time-resolution",
    "-t",
    metavar="SECS",
    help="Minimum time resolution in seconds",
    type=float,
    default=0.1,
)

args = parser.parse_args()

temperament: Temperament = eval(args.temperament, vars(tuning_tutor.pitch.temperament))

with open(args.abcfile) as f:
    text = f.read()
music = abcfile.parse(text).or_die()
target_offsets = np.array(list(note.offset for note in music.offsets))
target_freqs = temperament.to_pitch(target_offsets)

# Make sure the sound is in mono.
data, rate = read_wav(args.wavfile)
data = data.mean(axis=1)

_, results_iter = guess_frequencies_from_args(data, rate, args, display_progress=True)
freqs = np.array([freq for freq, _ in results_iter])

offsets = temperament.guess_offset(freqs)

out = np.zeros(len(target_freqs))
out_indices = np.repeat(-1, len(out))
j = (offsets == target_offsets[0]).nonzero()[0][0]
for i, (target_freq, target_offset) in enumerate(zip(target_freqs, target_offsets)):
    n = 0
    total = 0
    # XXX We here abuse the fact that control-flow constructs don't scope.
    for j in range(j, len(freqs)):
        # These have N frequencies/offsets.
        freq_slice = freqs[j]
        offset_slice = offsets[j]
        w = np.vstack((offset_slice == target_offset).nonzero()).T
        n += len(w)
        if len(w) and out_indices[i] == -1:
            out_indices[i] = j
        total += freq_slice.take(w).sum()
        if i + 2 < len(target_offsets) and any(offset_slice == target_offsets[i + 1]):
            if n != 0:
                out[i] = total / n
            break

print(
    f"Tuner finished with {len(out.nonzero()[0])} out of {len(target_offsets)} "
    "notes detected"
)
print(f"Note indices: {out_indices} (out of {len(offsets)} samples)")
