from argparse import ArgumentParser
import curses
from curses import wrapper

import numpy as np
import sounddevice as sd

import tuning_tutor.pitch.temperament
from tuning_tutor import analyze
from tuning_tutor.analyze import guess_frequencies_block
from tuning_tutor.pitch.temperament import Temperament
from tuning_tutor.scale import Pitch


def main(scr):
    parser = ArgumentParser(
        description="Tune interactively",
    )
    parser.add_argument(
        "--frequency-resolution",
        "-f",
        metavar="HZ",
        help="Minimum frequency resolution in Hz",
        type=float,
        default=1.0,
    )
    parser.add_argument(
        "--window",
        "-w",
        metavar="NAME",
        help="Windowing function to use",
        type=str,
        default="hamming",
    )
    parser.add_argument(
        "--temperament",
        "-T",
        metavar="TEMPERAMENT",
        help="Temperament to use",
        type=str,
        default="EqualTemperament(69, 440)",
    )
    parser.add_argument(
        "--rate", "-r", metavar="RATE", help="Sample rate", type=int, default=48000
    )
    parser.add_argument(
        "--threshold",
        "-t",
        metavar="AMPLITUDE",
        help="Threshold amplitude",
        type=float,
        default=5.0,
    )

    args = parser.parse_args()
    window_size = analyze.get_window_size(args.rate, args.frequency_resolution)
    temperament: Temperament = eval(
        args.temperament, vars(tuning_tutor.pitch.temperament)
    )

    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK)
    with sd.InputStream(samplerate=args.rate) as stream:
        while True:
            data, _ = stream.read(window_size)
            scr.clear()
            data = data.mean(axis=1).squeeze()
            (freq,), (amp,) = guess_frequencies_block(data, args.rate, 1)
            if amp >= args.threshold:
                scr.addstr(4, 0, f"Frequency: {freq} Hz")
                scr.addstr(5, 0, f"Amplitude: {amp}")
                offset = temperament.guess_offset(freq)
                notes = [pitch.display() for pitch in Pitch.from_offset(offset)]
                scr.addstr(0, 0, " / ".join(notes))
                target = temperament.to_pitch(offset)

                cents = 1200 * np.log2(target / freq)
                arrow = "↑" if cents < 0 else "↓"

                abs_cents = np.abs(cents)
                if abs_cents < 5:
                    color = curses.color_pair(1)
                elif abs_cents < 10:
                    color = curses.color_pair(2)
                else:
                    color = curses.color_pair(3)

                scr.addstr(1, 0, arrow, color)

                scr.addstr(2, 0, f"({abs_cents:.1f} cents)")

                scr.refresh()


wrapper(main)
